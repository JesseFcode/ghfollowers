//
//  UIVIew+Ext.swift
//  GHFollowers
//
//  Created by Jesse Frederick on 2/9/22.
//MARK: -Boilerplate-
import UIKit

extension UIView {
    
    /// Adds a subview.
    /// - Parameter views: The respective view that is being observed.
    func addSubviews(_ views: UIView...) { for view in views { addSubview(view) } }
    
    /// Constraints to pin an object to the edge of the screen.
    /// - Parameter superview: The respective view that is being observed.
    func pinToEdges(of superview: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: superview.topAnchor),
            leadingAnchor.constraint(equalTo: superview.leadingAnchor),
            trailingAnchor.constraint(equalTo: superview.trailingAnchor),
            bottomAnchor.constraint(equalTo: superview.bottomAnchor)
        ])
    }
}
