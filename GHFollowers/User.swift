//
//  User.swift
//  GHFollowers
//
//  Created by Jesse Frederick on 1/30/22.
//

import Foundation

/// Codable used for retrieving user info from the GitHubAPI.
struct User: Codable {
    let login: String
    let avatarUrl: String
    var name: String?
    var location: String?
    var bio: String?
    let publicRepos: Int
    let publicGists: Int
    let htmlUrl: String
    let following: Int
    let followers: Int
    let createdAt: Date
}
