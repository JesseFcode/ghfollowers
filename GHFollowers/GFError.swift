//
//  GFError.swift
//  GHFollowers
//
//  Created by Jesse Frederick on 2/2/22.
//

import Foundation

/// Enumeration of error messages that can occur in a alert container view.
enum GFError: String, Error {
    
    case invalidUsername = "This username created an invalid request. Please try again."
    case unableToComplete = "Unable to complete your request. Please check your internet connection"
    case invalidResponse = "Invalid response from the server. Please try again."
    case invalidData = "The data received from the server was invalid. Please try again."
    case favoriteError = "Error saving user to favorites data file. please try again"
    case favoriteErrorDuplicate = "You already have this GitHub user in your Favorites. You can not add the same person twice!"
}
