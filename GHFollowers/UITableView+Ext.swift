//
//  UITableView+Ext.swift
//  GHFollowers
//
//  Created by Jesse Frederick on 2/9/22.
//

import UIKit

extension UITableView {
    
    /// Reloads all new data to the main thread.
    func reloadDataOnMainThread() { DispatchQueue.main.async { self.reloadData() } }
    /// Removes any excess cells from view that are not being used on screen.
    func removeExcessCells() { tableFooterView = UIView(frame: .zero) }
}
