//
//  GFFollowerItemViewController.swift
//  GHFollowers
//
//  Created by Jesse Frederick on 2/3/22.
//

import UIKit

protocol GFFollowerVCDelegate: AnyObject {
    func didTapGetFollowers(for user: User)
}

class GFFollowerItemViewController: GFItemInfoViewController {
    
    weak var delegate: GFFollowerVCDelegate!
    
    
    init(user: User, delegate: GFFollowerVCDelegate) {
        super.init(user: user)
        self.delegate = delegate
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureItems()
    }
    
    
    /// Configuration for the "Get Followers" button on the User Info screen.
    private func configureItems() {
        itemInfoViewOne.set(itemInfoType: .followers, withCount: user.followers)
        itemInfoViewTwo.set(itemInfoType: .following, withCount: user.following)
        actionButton.set(backgroundColor: .systemGreen, title: "Get Followers")
    }
    
    
    /// Delegates a destination for the "Get Followers" button.
    override func actionButtonTapped() {
        delegate.didTapGetFollowers(for: user)
    }
}
