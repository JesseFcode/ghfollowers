//
//  SceneDelegate.swift
//  GHFollowers
//
//  Created by Jesse Frederick on 1/28/22.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {

        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        window                      = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene         = windowScene
        window?.rootViewController  = GFTabBarController()
        window?.makeKeyAndVisible()
        
        configureNavigationBar()
    }
    
    
    /// Configures the Navigation Bar appearance tint color as systemGreen.
    func configureNavigationBar() {
        UINavigationBar.appearance().tintColor = .systemGreen
    }
}
