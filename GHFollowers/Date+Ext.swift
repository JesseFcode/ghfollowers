//
//  Date+Ext.swift
//  GHFollowers
//
//  Created by Jesse Frederick on 2/3/22.
//
// MARK: -Boilerplate-

import Foundation

extension Date {
    
    /// Date formatter
    /// - Returns: String in date formatted from GitHub API
    func convertToMonthYearFormat() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM yyyy"
        return dateFormatter.string(from: self)
    }
}
