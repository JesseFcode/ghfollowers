//
//  PersistenceManager.swift
//  GHFollowers
//
//  Created by Jesse Frederick on 1/30/22.
//

import Foundation

/// Case of adding and removing users from the Favorites List.
enum PersistenceActionType {
    case add, remove
}

/// Implemented to manage refresh updates.
enum PersistenceManager {
    
    static private let defaults = UserDefaults.standard
    
    /// Returns updates of current favorites list in switch cases of adding and removing users from the list.
    /// - Parameters:
    ///   - favorite: Users that are in the favorites list.
    ///   - actionType: Cases of adding and removing users from favorites list.
    ///   - completion: Task has been completed with a return of the updated list.
    static func updateWith(favorite: Follower, actionType: PersistenceActionType, completion: @escaping (GFError?) -> Void) {
        retrieveFavorites { result in
            
            switch result {
            case .success(var favorites):
                
                switch actionType {
                case .add:
                    guard !favorites.contains(favorite) else {
                        completion(.favoriteErrorDuplicate)
                        return
                    }
                    favorites.append(favorite)
                case .remove:
                    favorites.removeAll { $0.login == favorite.login }
                }
                completion(save(favorites: favorites))
            case .failure(let error):
                completion(error)
            }
        }
    }
    
    
    /// Retrieves favorites data
    /// - Parameter completion: Task is completed with a return type.
    static func retrieveFavorites(completion: @escaping (Result<[Follower], GFError>) -> Void ) {
        guard let favoritesData = defaults.object(forKey: Keys.favorites) as? Data else {
            completion(.success([]))
            return
        }
        do {
            let decoder = JSONDecoder()
            let favorites = try decoder.decode([Follower].self, from: favoritesData)
            completion(.success(favorites))
        } catch {
            completion(.failure(.favoriteError))
        }
    }
    
    
    /// Caches current favorites added by user.
    /// - Parameter favorites: Users in the favorites list.
    /// - Returns: Nil if there are favorites to be saved. favoritesError if there are no favorites to be saved.
    static func save(favorites: [Follower]) -> GFError? {
        do {
            let encoder = JSONEncoder()
            let encdodedFavorites = try encoder.encode(favorites)
            defaults.setValue(encdodedFavorites, forKeyPath: Keys.favorites)
            return nil
        } catch {
            return .favoriteError
        }
    }
}
