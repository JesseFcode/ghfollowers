//
//  GFRepoItemVC.swift
//  GHFollowers
//
//  Created by Jesse Frederick on 2/3/22.
//

import UIKit

protocol GFRepoItemVCDelegate: AnyObject {
    func didTapGitHubProfile(for user: User)
}

class GFRepoItemViewController: GFItemInfoViewController {
    
    weak var delegate: GFRepoItemVCDelegate!
    
    
    init(user: User, delegate: GFRepoItemVCDelegate) {
        super.init(user: user)
        self.delegate = delegate
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureItems()
    }
    
    
    /// Configuration for the container view consisting of "Public Repos", "Public Gists", and the "GitHub Profile" button.
    private func configureItems() {
        itemInfoViewOne.set(itemInfoType: .repos, withCount: user.publicRepos)
        itemInfoViewTwo.set(itemInfoType: .gists, withCount: user.publicGists)
        actionButton.set(backgroundColor: .systemPurple, title: "GiHub Profile")
    }
    
    
    /// Delegates a destination for the "GitHub Profile" button.
    override func actionButtonTapped() {
        delegate.didTapGitHubProfile(for: user)
    }
}
