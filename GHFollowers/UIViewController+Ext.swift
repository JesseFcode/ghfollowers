//
//  UIViewController+Ext.swift
//  GHFollowers
//
//  Created by Jesse Frederick on 1/30/22.
//
// MARK: -Boilerplate-

import UIKit
import SafariServices

extension UIViewController {
    
    /// Presents alert container view.
    /// - Parameters:
    ///   - title: String containing the title for the alert container view.
    ///   - message: String containing the message for the alert container view.
    ///   - buttonTitle: String containing the button title for the alert container view.
    func presentGFAlertOnMainThread(title: String, message: String, buttonTitle: String) {
        DispatchQueue.main.async {
            let alertVC = GFAlertViewController(title: title, message: message, buttonTitle: buttonTitle)
            alertVC.modalPresentationStyle = .overFullScreen
            alertVC.modalTransitionStyle = .crossDissolve
            self.present(alertVC, animated: true)
        }
    }
    
    
    /// Presents Safari View Controller when the "GitHub Profile" button is tapped on the User Info screen.
    /// - Parameter url: URL that is being passed to the Safari view controller.
    func presentSafariViewController(with url: URL) {
        let safariViewController = SFSafariViewController(url: url)
        safariViewController.preferredControlTintColor = .systemGreen
        present(safariViewController, animated: true)
    }
}
