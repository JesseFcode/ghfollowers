//
//  FavoriteListViewController.swift
//  GHFollowers
//
//  Created by Jesse Frederick on 1/28/22.
//

import UIKit

class FavoriteListViewController: GFDataLoadingViewController {
    
    let tableView = UITableView()
    var favorites: [Follower] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewController()
        configureTableView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getFavorites()
    }
    
    
    /// Configuration for "Favorites" tabbar.
    func configureViewController() {
        view.backgroundColor = .systemBackground
        title = "Favorites"
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    
    /// Configuration for the table view on the Favorites screen.
    func configureTableView() {
        view.addSubview(tableView)
        tableView.frame = view.bounds
        tableView.rowHeight = 80
        tableView.delegate = self
        tableView.dataSource = self
        tableView.removeExcessCells()
        tableView.isAccessibilityElement = true
        tableView.accessibilityIdentifier = "Tableview"
        
        tableView.register(FavoriteTableViewCell.self, forCellReuseIdentifier: FavoriteTableViewCell.reuseID)
    }
    
    
    /// Retrieves favorites from the PersistenceManager.
    func getFavorites() {
        PersistenceManager.retrieveFavorites { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success(let favorites):
                self.updateUI(with: favorites)
                
            case .failure(let error):
                self.presentGFAlertOnMainThread(title: "Something went wrong.", message: error.rawValue, buttonTitle: "Ok")
            }
        }
    }
    
    
    /// Updates the UI if there are no favorites on the Favorites screen.
    /// - Parameter favorites: List of profiles that the user has favorited.
    func updateUI(with favorites: [Follower]) {
        if favorites.isEmpty {
            self.showEmptyStateView(with: "No Favorites?\nAdd one on the followers screen.", in: self.view)
        } else {
            self.favorites = favorites
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.view.bringSubviewToFront(self.tableView)
            }
        }
    }
}


extension FavoriteListViewController: UITableViewDataSource, UITableViewDelegate {
    /// Sets parameters for the table view based on number of favorites.
    /// - Parameters:
    ///   - tableView: The respective view that is being observed.
    ///   - section: The number of sections in the table view.
    /// - Returns: Returns number of rows in the section based on the favorites count.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favorites.count
    }
    
    
    /// Sets parameters for placing favorites in the cells of the table view.
    /// - Parameters:
    ///   - tableView: The respective view that is being observed.
    ///   - indexPath: The selected index of the respective view based on the cell the user tapped.
    /// - Returns: The cell object generated with a reuseID to present user profile data. This cell is returned to populate the table view with the respective user data given by the IndexPath.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FavoriteTableViewCell.reuseID) as! FavoriteTableViewCell
        let favorite = favorites[indexPath.row]
        cell.set(favorite: favorite)
        return cell
    }
    
    /// Selects a row on a given table view's IndexPath
    /// - Parameters:
    ///   - tableView: The respective view that is being observed.
    ///   - indexPath: The selected index of the respective view based on the cell the user tapped.
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let favorite = favorites[indexPath.row]
        let destinationViewController = FollowersListViewController(username: favorite.login)
        
        navigationController?.pushViewController(destinationViewController, animated: true)
    }
    
    /// Sets parameters for swiping and deleting a cell.
    /// - Parameters:
    ///   - tableView: The respective view that is being observed.
    ///   - editingStyle: Used for the "Delete" editing style.
    ///   - indexPath: The selected index of the respective view based on the cell the user wishes to delete.
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else { return }
        
        PersistenceManager.updateWith(favorite: favorites[indexPath.row], actionType: .remove) { [weak self] error in
            guard let self = self else { return }
            guard let error = error else {
                self.favorites.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
                return
            }
            self.presentGFAlertOnMainThread(title: "Unable to remove", message: error.rawValue, buttonTitle: "Ok")
        }
    }
}
