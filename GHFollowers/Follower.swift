//
//  Follower.swift
//  GHFollowers
//
//  Created by Jesse Frederick on 1/30/22.
//

import Foundation

/// Codable for user login and URL for the avatar image.
struct Follower: Codable, Hashable {
    var login: String
    var avatarUrl: String
}
