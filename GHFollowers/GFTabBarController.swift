//
//  GFTabBarController.swift
//  GHFollowers
//
//  Created by Jesse Frederick on 2/7/22.
//

import UIKit

class GFTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        UITabBar.appearance().tintColor = .systemGreen
        viewControllers = [createSearchNC(), createFavoritesNC()]
    }
    
    
    /// Configuration for the "Search" tabbar item.
    /// - Returns: "Search" tabbar
    func createSearchNC() -> UINavigationController {
        let searchVC = SearchViewController()
        searchVC.title = "Search"
        searchVC.tabBarItem = UITabBarItem(tabBarSystemItem: .search, tag: 0)
        
        return UINavigationController(rootViewController: searchVC)
    }
    
    
    /// Configuration for the "Favorites" tabbar item.
    /// - Returns: "Favorites" tabbar
    func createFavoritesNC() -> UINavigationController {
        let favoritesListVC = FavoriteListViewController()
        favoritesListVC.title = "Favorites"
        favoritesListVC.tabBarItem = UITabBarItem(tabBarSystemItem: .favorites, tag: 1)
        
        return UINavigationController(rootViewController: favoritesListVC)
    }
}
