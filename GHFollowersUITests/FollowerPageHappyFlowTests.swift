//
//  FollowerPageHappyFlowTests.swift
//  GHFollowersUITests
//
//  Created by Jesse Frederick on 2/13/22.
//

import XCTest

class FollowerPageHappyFlowTests: ParentTest {

    override func setUp() {
        app.launch()
        continueAfterFailure = false
    }
    
    /// Tests to ensure the "Add Favorites" button works. Login is initialized with the testInput("Sallen0400") and the addFavoritesButton is tapped.
    func testAddToFavoritesButton() {
        login("Sallen0400")
        testFollowerScreen.addFavoritesButton.tap()
    }
    
    /// Tests a successful added favorite and alert box. clearFavoriteScreen is initialized to clear any cells that exist in the Favorites Screen. Login is then initialized with the testInput("Sallen0400") and the addFavoritesButton is tapped. Then waitForAddedFavorite is initialized and the alert box is handled through this function.
    func testAddedFavoriteSuccessModalView() {
        clearFavoriteScreen()
        login("Sallen0400")
        testFollowerScreen.addFavoritesButton.tap()
        waitForAddedFavorite()
    }
    
    /// Tests to ensure the Safari modal view works. Login is initialized with the testInput("Sallen0400"). waitForDyang is initialized to ensure the existence of the user cell; once it exists, the cell is tapped. tapOnGitHubProfileButton is tapped and the urlElement is tapped once the Safari modal view is visibile.
    func testSafariView() {
        login("Sallen0400")
        waitForDyang()
        testFollowerScreen.tapOnGitHubProfileButton.tap()
//        testFollowerScreen.urlElement.tap()
        testFollowerScreen.webViewDoneButton.tap()
        waitForGetFollowersButton()
    }
    
    /// Tests to ensure scrolling on the collection view works. Login is initialized with the testInput("Sallen0400"). Once the Follower Screen is visible, scrollUp is used to find the tapOnUserConnectionDigital element.
    func testCollectionScroll() {
        login("Sallen0400")
        app.scrollUp(element: testFollowerScreen.tapOnUserKrazyowl)
    }
    
    /// Tests the "Get Followers" button within user modal view. Login is initialized with the testInput("Sallen0400"). tapOnUserDyang is tapped, followed by the getFollowersButton being tapped. The collection view is then scrolled up to find tapOnUserXyxdasnjss element.
    func testUserInfoGetFollowers() {
        login("Sallen0400")
        testFollowerScreen.tapOnUserDyang.tap()
        testFollowerScreen.getFollowersButton.tap()
        app.scrollUp(element: testFollowerScreen.tapOnUserEye008)
    }
    
    /// Tests to ensure the "Search" text field exists. Login is initialized with the testInput("Sallen0400"). Once the Followers collection view is visible, scrollDown is used to reveal the searchField. searchField is then tapped and typeText is used with the string value "K". ifKeyboardExists is initialized to hide the keyboard and scrollUp is used to find tapOnUserConnectionDigital element.
    func testFollowersSearchTextField() {
        login("Sallen0400")
        app.scrollDown(element: testFollowerScreen.searchField)
        testFollowerScreen.searchField.tap()
        testFollowerScreen.searchField.typeText("K")
        ifKeyboardExists()
        testFollowerScreen.tapOnUserKrazyowl.tap()
        testFollowerScreen.getFollowersButton.tap()
    }
}




func testElements() {
    
    
    
}







