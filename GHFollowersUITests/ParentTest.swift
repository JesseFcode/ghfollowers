//
//  ParentTest.swift
//  GHFollowersUITests
//
//  Created by Jesse Frederick on 3/19/22.
//

import XCTest

class ParentTest: XCTestCase {
    let app = XCUIApplication()
    var testLoginScreen = LoginScreen()
    var testFollowerScreen = FollowerScreen()
    var testFavoritesScreen = FavoritesScreen()
    
    public lazy var hideKeyboard = app.keyboards.buttons["Hide keyboard"]
    public lazy var goKeyboard = app.keyboards.buttons["Go"]
    public lazy var keyboardDelete = app.keys["delete"]
    public lazy var searchKeyboard = app.buttons["search"]
    
    /// Hides keyboard
    func performHideKeyboard() {
        hideKeyboard.tap()
    }
    
    /// Taps "Go" on keyboard
    func performGoKeyboard() {
        goKeyboard.tap()
    }
    
    /// Performs if "Search" is in place for "Go" on keyboard
    func ifKeyboardExists() {
        if searchKeyboard.exists {
            searchKeyboard.tap()
        }
    }
    
    func waitForGetFollowersButton() {
        let followerButtonExists = testLoginScreen.getFollowersButton.waitForExistence(timeout: 10)
        if followerButtonExists {
            testLoginScreen.getFollowersButton.tap()
        }
    }
    
    /// Waits for "Add Favorites" Button to exist. If the button exists, then the button is tapped.
    func waitForAddFavoriteButton() {
        let addButtonExists = testFollowerScreen.addFavoritesButton.waitForExistence(timeout: 10)
        if addButtonExists {
            testFollowerScreen.addFavoritesButton.tap()
        }
    }
    
    /// Waits for "Success" static text to exist. If the static text exists, then handleAddedFavoritesModalView initializes.
    func waitForAddedFavorite() {
        let addedFavoritesExists = testFollowerScreen.successStaticText.waitForExistence(timeout: 10)
        if addedFavoritesExists {
            handleAddFavoritesModalView()
        }
    }
    
    /// Waits for keyboard "Go" button to exist. If the "Go" button exists, then types a string into the text field, taps delete on keyboard and taps the "Go" button.
    func waitForKeyboardGo() {
        let keyboardGoExists = goKeyboard.waitForExistence(timeout: 10)
        if keyboardGoExists {
            testLoginScreen.enterAUsernameTextField.typeText("J")
            keyboardDelete.tap()
            performGoKeyboard()
        }
    }
    
    /// Waits for user "Dyang" cell to exist. If the user cell exists, then the cell is tapped.
    func waitForDyang() {
        let dyangExists = testFollowerScreen.tapOnUserDyang.waitForExistence(timeout: 10)
        if dyangExists {
            testFollowerScreen.tapOnUserDyang.tap()
        }
    }
    
    /// Allows user to login
    /// - Parameter testInput:The string object to pass into function
    func login(_ testInput: String) {
        testLoginScreen.enterAUsernameTextField.tap()
        testLoginScreen.enterAUsernameTextField.typeText(testInput)
        performGoKeyboard()
    }
    
    /// Handles alert box for Empty Username entry. emptyUsernameStaticText, bodyStaticText, and okButton is tapped.
    func handleEmptyNameTextFieldErrorModalView() {
        testLoginScreen.emptyUsernameStaticText.tap()
        testLoginScreen.bodyStaticText.tap()
        testLoginScreen.okButton.tap()        
    }
    
    /// Handles alert box for Added Favorite. successStaticText, successBodyText, and hoorayButton is tapped.
    func handleAddFavoritesModalView() {
        testFollowerScreen.successStaticText.tap()
        testFollowerScreen.successBodyText.tap()
        testFollowerScreen.hoorayButton.tap()
    }
    
    /// Handles alert box if user has already been added to Favorites. somethingWentWrongStaticText, somethingWentWrongBodytext, and okButton is tapped.
    func handleAlreadyAddedErrorModalView() {
        testFollowerScreen.somethingWentWrongStaticText.tap()
        testFollowerScreen.somethingWentWrongBodyText.tap()
        testLoginScreen.okButton.tap()
    }
    
    /// Clears Favorite screen with "Sallen0400" cell. The favoritesTabBar is tapped. If the sAllen0400Cell exists, then swipe left on the cell until the deleteCellButton is visibile and tap it. Afterwards the searchTabBarButton is tapped. If the sAllen0400Cell does not exist, then the searchTabBarButton is tapped.
    func clearFavoriteScreen() {
        testLoginScreen.favoritesTabBar.tap()
        // if no cell is available, switch to login screen
        if testFavoritesScreen.sAllen0400Cell.exists {
            testFavoritesScreen.sAllen0400Cell.swipeLeft(velocity: XCUIGestureVelocity(300))
            testFavoritesScreen.deleteCellButton.tap()
            testLoginScreen.searchTabBarButton.tap()
        } else {
            testLoginScreen.searchTabBarButton.tap()
        }
        
    }
}

extension XCUIElement {
    @discardableResult
    func scrollDown(element: XCUIElement) -> XCUIElement {
        while !element.exists {
            swipeDown()
        }
        return element
    }
    func scrollUp(element: XCUIElement) -> XCUIElement {
        while !element.exists {
            swipeUp()
        }
        return element
    }
}





