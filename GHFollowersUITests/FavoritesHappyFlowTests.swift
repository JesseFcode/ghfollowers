//
//  FavoritesHappyFlowTests.swift
//  GHFollowersUITests
//
//  Created by Jesse Frederick on 3/1/22.
//

import XCTest

class FavoritesHappyFlowTests: ParentTest {
    
    override func setUp() {
        app.launch()
        continueAfterFailure = false
    }
    
    /// Tests to ensure the table view on the "Favorites" screen exists. The clearFavoriteScreen is initialized to delete any cells that exist, then login is initialized with the testInput("Sallen0400"). The addFavoritesButton is tapped and the handleAddFavoritesModalView is initialized to handle the alert box. The favoritesTabBar is tapped and the customTableView is tapped to test the existence of the table.
    func testTableView() {
        clearFavoriteScreen()
        login("Sallen0400")
        testFollowerScreen.addFavoritesButton.tap()
        handleAddFavoritesModalView()
        testLoginScreen.favoritesTabBar.tap()
        testFavoritesScreen.customTableView.tap()
    }
}
