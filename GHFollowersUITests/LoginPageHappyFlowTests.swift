//
//  GHFollowersUITests.swift
//  GHFollowersUITests
//
//  Created by Jesse Frederick on 2/12/22.
//

import XCTest

class LoginPageHappyFlowTests: ParentTest {

    override func setUp() {
        app.launch()
        continueAfterFailure = false
    }
    
    /// Tests "Empty Username" text field alert box. validateLoginScreenUILogoAndKeyboard is initialized and XCTAssert is used for the enterAUsernameTextField to ensure it exists. enterAUsernameTextField is then tapped and waitForKeyboardGo is initialized to waitForExistence and tap on the "Go" button on the keyboard. handleEmptyNameTextFieldErrorModalView is initalized to handle the alert box.
    func testErrorHandlingLoginScreen() {
        testLoginScreen.validateLoginScreenUILogoAndKeyboard()
        XCTAssert(testLoginScreen.enterAUsernameTextField.exists)
        testLoginScreen.enterAUsernameTextField.tap()
        waitForKeyboardGo()
        handleEmptyNameTextFieldErrorModalView()
    }
    
    /// Tests searching a username and empty state. Login is initialized with the testInput("Jessef97") and noFollowersStaticText is tapped for existence on the empty state screen.
    func testSearchingUsernameLoginScreen() {
        login("Jessef97")
        testLoginScreen.noFollowersStaticText.tap()
    }
    
    /// Tests to ensure the TabBar works. favoritesTabBar is tapped, followed by searchTabBarButton being tapped.
    func testTabBar() {
        testLoginScreen.favoritesTabBar.tap()
        testLoginScreen.searchTabBarButton.tap()
    }
    
    /// Tests to ensure the "Get Followers" button functions properly by tapping on getFollowersButton.
    func testGetFollowers() {
        testLoginScreen.getFollowersButton.tap()
    }
}



