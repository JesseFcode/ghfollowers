//
//  FavoritesScreen.swift
//  GHFollowersUITests
//
//  Created by Jesse Frederick on 3/1/22.
//

import XCTest

class FavoritesScreen: XCTestCase {

    public lazy var customTableView: XCUIElement = XCUIApplication().tables["Tableview"]
    public lazy var sAllen0400Cell: XCUIElement = XCUIApplication().tables["Tableview"].cells["SAllen0400"].children(matching: .other).element(boundBy: 1)
    public lazy var krazyOwlCell: XCUIElement = XCUIApplication().tables["Tableview"].cells["krazyowl"].children(matching: .other).element(boundBy: 1)
    public lazy var deleteCellButton: XCUIElement = XCUIApplication().tables.buttons["Delete"]
}




