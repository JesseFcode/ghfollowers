//
//  FollowerScreen.swift
//  GHFollowersUITests
//
//  Created by Jesse Frederick on 2/13/22.
//

import XCTest

class FollowerScreen: XCTestCase {

    public lazy var addFavoritesButton: XCUIElement = XCUIApplication().navigationBars.buttons["Add"]
    public lazy var successStaticText: XCUIElement = XCUIApplication().staticTexts["Success!"]
    public lazy var successBodyText: XCUIElement = XCUIApplication().staticTexts["You have successfully favorited this user 🎉"]
    public lazy var hoorayButton: XCUIElement = XCUIApplication().buttons["Hooray!"]
    public lazy var somethingWentWrongStaticText: XCUIElement = XCUIApplication().staticTexts["Something went wrong"]
    public lazy var somethingWentWrongBodyText: XCUIElement = XCUIApplication().staticTexts["You already have this GitHub user in your Favorites. You can not add the same person twice!"]
    public lazy var tapOnUserDyang: XCUIElement = XCUIApplication().collectionViews.cells.containing(.staticText, identifier:"dyang").element
    public lazy var tapOnUserConnectionDigital: XCUIElement = XCUIApplication().collectionViews.cells.containing(.staticText, identifier:"ConnectionsDigital").element
    public lazy var tapOnUserEye008: XCUIElement = XCUIApplication().collectionViews.cells.containing(.staticText, identifier:"eye008").element
    public lazy var tapOnUserKrazyowl: XCUIElement = XCUIApplication().collectionViews.cells.containing(.staticText, identifier:"krazyowl").element
    public lazy var tapOnGitHubProfileButton: XCUIElement = XCUIApplication().scrollViews.otherElements.staticTexts["GiHub Profile"]
    public lazy var getFollowersButton: XCUIElement = XCUIApplication().scrollViews.otherElements.buttons["Get Followers"]
    public lazy var urlElement: XCUIElement = XCUIApplication().otherElements["URL"]
    public lazy var searchField: XCUIElement = XCUIApplication().navigationBars["Sallen0400"].searchFields["Search for a username"]
    public lazy var webViewDoneButton: XCUIElement = XCUIApplication().buttons["Done"]
    public lazy var cellView: XCUIElement = XCUIApplication().collectionViews.children(matching:.any).element(boundBy: 1)
}






