//
//  GHFollowersUserJourney.swift
//  GHFollowersUITests
//
//  Created by Jesse Frederick on 3/30/22.
//

import XCTest

class GHFollowersUserJourney: ParentTest {
    
    override func setUp() {
        app.launch()
        continueAfterFailure = false
    }
    
    /// End-to-end (E2E) of user experience. clearFavoritesScreen is initalized to clear any favorites. Login is then initialized with the testInput("Sallen0400"). Once login is complete, waitForAddFavoritesButton is initialized then tapped and handleAddFavoritesModal handles the alert box. scrollUp is then used to scroll to tapOnUserKrazyowl then it's cell is tapped. The tapOnGitHubProfileButton is tapped. Once the Safari modal view is visible, the webViewDoneButton is tapped and the getFollowersButton is tapped. The addFavoritesButton is tapped and the handleAddFavoritesModalView is initialized to handle the alert box. To end the function, the favoritesTabBar is tapped and the krazyOwlCell on the table view is swiped left and the deleteCellButton is tapped to delete the cell.
    func testUserJourney() {
        clearFavoriteScreen()
        login("Sallen0400")
        waitForAddFavoriteButton()
        handleAddFavoritesModalView()
        app.scrollUp(element: testFollowerScreen.tapOnUserKrazyowl)
        testFollowerScreen.tapOnUserKrazyowl.tap()
        testFollowerScreen.tapOnGitHubProfileButton.tap()
        app.buttons["Done"].coordinate(withNormalizedOffset: CGVector(dx: 0.5, dy: 0.5)).tap()
        testFollowerScreen.getFollowersButton.tap()
        testFollowerScreen.addFavoritesButton.tap()
        handleAddFavoritesModalView()
        testLoginScreen.favoritesTabBar.tap()
        testFavoritesScreen.krazyOwlCell.swipeLeft(velocity: 300)
        testFavoritesScreen.deleteCellButton.tap()
    }
}
