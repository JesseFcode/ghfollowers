//
//  LoginScreen.swift
//  GHFollowersUITests
//
//  Created by Jesse Frederick on 2/12/22.
//

import XCTest

class LoginScreen: XCTestCase {
    
    public lazy var githubFollowersLogo: XCUIElement = XCUIApplication().windows.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
    public lazy var enterAUsernameTextField: XCUIElement = XCUIApplication().textFields["Enter a username"]
    public lazy var getFollowersStaticText: XCUIElement = XCUIApplication().staticTexts["Get Followers"]
    public lazy var getFollowersButton: XCUIElement = XCUIApplication().buttons["Get Followers"]
    public lazy var searchTabBarButton: XCUIElement = XCUIApplication().tabBars["Tab Bar"].buttons["Search"]
    public lazy var favoritesTabBar: XCUIElement = XCUIApplication().tabBars["Tab Bar"].buttons["Favorites"]
    public lazy var emptyUsernameStaticText: XCUIElement = XCUIApplication().staticTexts["Empty Username"]
    public lazy var bodyStaticText: XCUIElement = XCUIApplication().staticTexts["Please enter a username. We need to know who to look for 😀."]
    public lazy var okButton: XCUIElement = XCUIApplication().buttons["Ok"]
    public lazy var noFollowersStaticText: XCUIElement = XCUIApplication().staticTexts["This user doesn't have any followers. Go follow them 😀."]
    
    /// Validates that the githubFollowersLogo is not nil.
    func validateLoginScreenUILogoAndKeyboard() {
        XCTAssertNotNil(githubFollowersLogo.exists)
    }
}
     
